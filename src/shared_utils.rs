use std::ops::{Index, IndexMut};

#[derive(Clone,Copy, PartialEq)]
pub enum MovementCollision {
    None,
    Collision(Movement,f32,f32),
}

#[derive(Clone, Copy,PartialEq,Debug)]
pub enum Movement
{
    Up = 0,
    Left,
    Right, 
    Down,
    UpLeft,
    UpRight,
    DownLeft,
    DownRight,
    Idle,
}

const MOVEMENT_ENUM_SIZE:usize = Movement::Idle as usize + 1;

impl<T> Index<Movement> for [T; MOVEMENT_ENUM_SIZE]
{
    type Output = T;
    fn index(&self, movement:Movement) -> &Self::Output {
        &self[movement as usize]
    }
}

impl<T> IndexMut<Movement> for [T; MOVEMENT_ENUM_SIZE]
{
    fn index_mut(&mut self, index: Movement) -> &mut Self::Output {
        &mut self[index as usize]
    }
}