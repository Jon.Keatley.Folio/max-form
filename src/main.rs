use macroquad::prelude::*;

use resources::{
    load_resources,
    Resources
};

use player::{
    Player,
    PlayerMode,
};

use tilemap::{
    TileMap,
    //TileEffect,
    TileType,
    Grid,
    //LoopMode,
};

use pewpew::PewPew;
use baddies::Baddie;

//use shared_utils::Collision;

mod resources;
mod player;
mod sprite;
mod tilemap;
mod shared_utils;
mod pewpew;
mod baddies;


fn conf() -> Conf {
    Conf {
        window_title: String::from("Maximum Transform"),
        window_width: 1260,
        window_height: 768,
        fullscreen: false,
        ..Default::default()
    }
}


#[macroquad::main(conf)]
async fn main() {
    clear_background(BLACK);

    let resources:Resources = load_resources().await;
    
    let game_bounds = Rect::new(0., 0.,screen_width(), screen_height());
    
    //setup player
    let mut player = Player::new(
         resources.player,
        Vec2::new(screen_width() / 2., screen_height() / 2.),
        Vec2::new(0.,0.),
    );
    player.change_mode(PlayerMode::Idle);
    
    //setup map
    let tile_count:usize = 39 * 24;
    
    let mut tilemap = TileMap::new(
        Grid{width:39,height:24},
        Grid{width:32,height:32},
        resources.tiles,
        Vec2::new(16.,16.),
        Vec2::new(0.,0.),
    );
    
    tilemap.update_tile(68, TileType::Solid);
    tilemap.update_tile(42,TileType::Solid);
    tilemap.update_tile(43,TileType::Solid);
    tilemap.update_tile(44,TileType::Solid);
    
    let mut map_data:Vec<usize> = vec![20;tile_count];
    
    map_data[40] = 42;
    map_data[ 1] = 68;
    
    map_data[ 127] = 68;
    map_data[ 166] = 68;
    map_data[ 205] = 68;
    map_data[ 244] = 68;
    map_data[ 283] = 68;
    map_data[ 322] = 68;
    map_data[ 361] = 43;
    
    map_data[ 167] = 44;
    map_data[ 168] = 42;
    map_data[ 169] = 43;
    
    map_data[ 512] = 5;
    map_data[ 513] = 6;
    map_data[ 514] = 7;
    
    map_data[ 551] = 19;
    map_data[ 553] = 21;
    
    map_data[ 590] = 33;
    map_data[ 591] = 34;
    map_data[ 592] = 35;
    
    map_data[ 0] = 0;
    for n in map_data.iter_mut().take(39).skip(2)
    {
        *n = 0;
    }
    /*for n in 0..84
    {
        map_data[n] = n;
    }*/

    tilemap.set_map(&map_data);
    
    
    //setup pewpew
    let mut all_the_pews:Vec<PewPew> = Vec::new();
    
    //setup baddies
    let mut all_the_baddies:Vec<Baddie> = Vec::new();
    
    all_the_baddies.push(
        Baddie::new(
            resources.baddie,
            Vec2::new(screen_width() / 2., screen_height() / 2.),
            Vec2::new(0.,0.)
            )
    );
 
    //start game loop
    loop {

        let delta: f32 = get_frame_time();    
        //update elements

        tilemap.update(delta);
        
        tilemap.draw();
        
        //collision
        player.move_player(delta);
        let player_bounds = player.get_bounds();  
        let movement_collision = tilemap.has_collision(player_bounds, player.get_movement());
        
        player.update(delta, movement_collision);


        //draw 
        //draw_rectangle(player_bounds.x,player_bounds.y,player_bounds.w,player_bounds.h,RED);
        player.draw();
        
        if let Some(pew_direction) = player.wants_to_pew()
        {
            all_the_pews.push(
                PewPew{ 
                    pos:Vec2::new(player.get_bounds().x,player.get_bounds().y),
                    vel:pew_direction,
                    life:1.,
                    can_cull:false,
                }
            )
        }
        
        for baddie in &mut all_the_baddies
        {
            baddie.update(delta);
            baddie.draw();
        }
        
        for pew in &mut all_the_pews
        {
            pew.update(delta, game_bounds);
            pew.draw();
        }
        
        all_the_pews.retain(|p| !p.can_cull); //remove old pews
        //draw_rectangle(player_bounds.x,player_bounds.y,1.,1.,RED);
        
        next_frame().await  
    }
}