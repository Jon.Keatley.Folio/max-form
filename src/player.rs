use macroquad::prelude::*;

use crate::{
    sprite::{
        Sprite,
        Frame,
        AnimationStyle},
    shared_utils::{
        Movement, MovementCollision,
    },
}; 

#[derive(Clone, Copy)]
pub enum PlayerMode 
{
    Idle = 0,
    WalkLeft,
    WalkRight,
    WalkUp,
    WalkDown,
}

pub struct Player
{
    common:Sprite,
    mode:PlayerMode,
    movement:Movement,
    fire_cool_down:f32,
    request_fire:bool,
}

const FIRE_LIMIT:f32 = 0.25;

pub const KEY_RIGHT: KeyCode = KeyCode::Right;
pub const KEY_LEFT: KeyCode = KeyCode::Left;
pub const KEY_UP: KeyCode = KeyCode::Up;
pub const KEY_DOWN: KeyCode = KeyCode::Down;
pub const KEY_FIRE: KeyCode = KeyCode::Space;

pub const WALK_SPEED: f32 = 164.;

impl Player
{
    pub fn new(texture:Texture2D, pos:Vec2, vel:Vec2) -> Player
    {
        let mut s:Sprite = Sprite::new(
            texture,
            pos,
            vel,
        );
        
        let idle:[Frame; 1] = [
            Frame::new(1.,96.,0.,24.,24.)
        ];
        s.add_animation(PlayerMode::Idle as usize,AnimationStyle::Loop, &idle); 
        
        let walk_right:[Frame; 4] = [
        Frame::new(0.25,24.,0.,24.,24.),
        Frame::new(0.2,24.,24.,24.,24.),
        Frame::new(0.25,24.,48.,24.,24.),    
        Frame::new(0.2,24.,24.,24.,24.),
        ];
        s.add_animation(PlayerMode::WalkRight as usize,AnimationStyle::Loop, &walk_right);
        
        let walk_left:[Frame; 4] = [
        Frame::new(0.25,72.,0.,24.,24.),
        Frame::new(0.2,72.,24.,24.,24.),
        Frame::new(0.25,72.,48.,24.,24.),    
        Frame::new(0.2,72.,24.,24.,24.),
        ];
        s.add_animation(PlayerMode::WalkLeft as usize,AnimationStyle::Loop, &walk_left);

        let walk_up:[Frame; 4] = [
        Frame::new(0.25,0.,0.,24.,24.),
        Frame::new(0.2,0.,24.,24.,24.),
        Frame::new(0.25,0.,48.,24.,24.),    
        Frame::new(0.2,0.,24.,24.,24.),
        ];
        s.add_animation(PlayerMode::WalkUp as usize,AnimationStyle::Loop, &walk_up);
        
        let walk_down:[Frame; 4] = [
        Frame::new(0.25,48.,0.,24.,24.),
        Frame::new(0.2,48.,24.,24.,24.),
        Frame::new(0.25,48.,48.,24.,24.),    
        Frame::new(0.2,48.,24.,24.,24.),
        ];
        s.add_animation(PlayerMode::WalkDown as usize,AnimationStyle::Loop, &walk_down);
        
        s.change_animation(PlayerMode::Idle as usize);
           
        Player {
          common:s,
          mode:PlayerMode::Idle,
          movement:Movement::Idle,
          request_fire:false,
          fire_cool_down:0.,
        }
    }
    
    pub fn wants_to_pew(&mut self)->Option<Vec2> 
    {
        let want = self.request_fire;
        self.request_fire = false;
        if want
        {
            let speed: f32 = 100.;
            let mouse = mouse_position();
            let diff_a = mouse.0 - self.common.pos.x;
            let diff_b = mouse.1 - self.common.pos.y;
            let angle = f32::atan2(diff_b,diff_a);
            let vx = f32::cos(angle) * speed;
            let vy = f32::sin(angle) * speed;
            
            return Some(Vec2::new(vx,vy));
        }
        
        None
    }
    
    pub fn change_mode(&mut self, s:PlayerMode)
    {
        self.mode = s;
        self.common.change_animation(self.mode as usize);
    }
    
    pub fn move_player(&mut self, delta:f32)
    {
        self.common.update_x(delta);
        self.common.update_y(delta);
    }
    
    pub fn update(&mut self, delta:f32,collision:MovementCollision)
    {
        let mut input:Vec2 = Vec2::new(0.,0.);
        let mut desired_movement = Movement::Idle;
        
        if self.fire_cool_down > 0.
        {
            self.fire_cool_down-= delta;
        }
        
        //pewpew
        if is_mouse_button_down(MouseButton::Left)&& self.fire_cool_down <= 0.
        {
           self.request_fire = true;
           self.fire_cool_down = FIRE_LIMIT;
        }

        // x
        if is_key_down(KEY_LEFT)
        {
            desired_movement = Movement::Left;
        }
        
        if is_key_down(KEY_RIGHT)
        {
            desired_movement = Movement::Right;
        }
        
        // y
        if is_key_down(KEY_UP)
        {
            match desired_movement
            {
                Movement::Left => desired_movement = Movement::UpLeft,
                Movement::Right => desired_movement = Movement::UpRight,
                _ => desired_movement = Movement::Up
            }
        }
        
        if is_key_down(KEY_DOWN)
        {
            match desired_movement
            {
                Movement::Left => desired_movement = Movement::DownLeft,
                Movement::Right => desired_movement = Movement::DownRight,
                _ => desired_movement = Movement::Down
            }
        }
        
        match collision
        {
            MovementCollision::None => {  },
            MovementCollision::Collision(m,_ox ,_oy ) => 
            {
                match m 
                {
                    Movement::Up => {
                        self.common.undo_update_y(delta);
                        
                        match desired_movement
                        {
                            Movement::Up=> { desired_movement = Movement::Up },
                            Movement::UpLeft => { desired_movement = Movement::Left},
                            Movement::UpRight => {desired_movement = Movement::Right},
                            _ => {}
                        }
                    },
                    Movement::Left => {
                        self.common.undo_update_x(delta);
                        
                        match desired_movement
                        {
                            Movement::Left=> { desired_movement = Movement::Left },
                            Movement::UpLeft => { desired_movement = Movement::Up},
                            Movement::DownLeft => {desired_movement = Movement::Down},
                            _ => {},
                        }
                    },
                    Movement::Right => {
                        self.common.undo_update_x(delta);
                        
                        match desired_movement
                        {
                            Movement::Right=> { desired_movement = Movement::Right },
                            Movement::UpRight => { desired_movement = Movement::Up},
                            Movement::DownRight => {desired_movement = Movement::Down},
                            _ => {}
                        }
                    },
                    Movement::Down => {
                        self.common.undo_update_y(delta);
                        match desired_movement
                        {
                            Movement::Down=> { desired_movement = Movement::Down },
                            Movement::DownLeft => { desired_movement = Movement::Left},
                            Movement::DownRight => {desired_movement = Movement::Right},
                            _ => {}
                        }
                    },
                    _ => {
                        //collision occured on both axis
                        //THIS IS THE ISSUE. SLIDING CANT HAPPEN BECAUSE MOVEMENT IS TAKEN AS COLLISION POINT RATHER THAN RELATIVE TILE
                        //DOESENT RESOLVE LEFT

                       // desired_movement = Movement::Idle;
                        self.common.undo_update_x(delta);
                        self.common.undo_update_y(delta);
                    },
                }
            }
        }
        
        if self.movement != desired_movement
        {
            //println!("Direction change {:?}", desired_movement);
            self.movement = desired_movement;
            
            //println!("Direction {:?}",self.movement);
            match self.movement
            {
                Movement::Up => { input.y -= WALK_SPEED; self.change_mode(PlayerMode::WalkUp);   },
                Movement::Down => { input.y += WALK_SPEED; self.change_mode(PlayerMode::WalkDown);   },
                Movement::Left => { input.x -= WALK_SPEED; self.change_mode(PlayerMode::WalkLeft);  },
                Movement::Right => { input.x += WALK_SPEED; self.change_mode(PlayerMode::WalkRight);   },
                
                Movement::UpLeft => { input.y -= WALK_SPEED; input.x -= WALK_SPEED; self.change_mode(PlayerMode::WalkLeft);   },
                Movement::DownLeft => { input.y += WALK_SPEED; input.x -= WALK_SPEED; self.change_mode(PlayerMode::WalkLeft);   },
                Movement::UpRight => { input.y -= WALK_SPEED; input.x += WALK_SPEED; self.change_mode(PlayerMode::WalkRight);   },
                Movement::DownRight => { input.y += WALK_SPEED; input.x += WALK_SPEED; self.change_mode(PlayerMode::WalkRight);   },
                _ => {self.change_mode(PlayerMode::Idle);}    
            }
            
            self.common.vel.x = input.x;
            self.common.vel.y = input.y;
        }
        
        self.common.update_animation(delta);
    }
    
  
    pub fn draw(&mut self)
    {
        self.common.draw();
    }
    
    pub fn get_bounds(&self) -> Rect
    {
        self.common.get_bounds()
    }
    
    pub fn get_movement(&self) -> Movement 
    {
        self.movement
    }
}