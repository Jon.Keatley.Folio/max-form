use macroquad::prelude::*;
//use macroquad::math::{Rect,Vec2};

pub struct PewPew
{
    pub pos:Vec2, 
    pub vel:Vec2,
    pub life:f32,
    pub can_cull:bool,
}

impl PewPew{
    
    pub fn update(&mut self, delta:f32,bounds:Rect)
    {
        
        self.pos.x += self.vel.x * delta;
        self.pos.y += self.vel.y * delta;
        
        self.life -= delta;
        
        //check bounds to kill - if we use life do we care about bounds?
        self.can_cull = !bounds.contains(self.pos);
        
    }
    
    pub fn draw(&self)
    {
        draw_circle(self.pos.x, self.pos.y, 1.2, BLACK);
    }
}