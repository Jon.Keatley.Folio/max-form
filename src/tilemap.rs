use macroquad::shapes::draw_rectangle;
use macroquad::texture::{Texture2D, draw_texture_ex, DrawTextureParams};
use macroquad::math::f32::Vec2;
use macroquad::color::{colors, GREEN, MAGENTA, PINK};
use macroquad::math::Rect;
//use macroquad::prelude::*;

use crate::shared_utils::{Movement, MovementCollision};


#[derive(Clone, Copy, PartialEq)]
pub enum TileType {
    Empty,
    Solid,
}

/*#[derive(Clone, Copy)]
pub enum TileEffect {
    Normal,
    FlipX,
    FlipY,
    FlipBoth
}*/

/*pub enum LoopMode {
    StopBoth,
    LoopVStopH,
    LoopBoth,
    StopVLoopH,
}*/

#[derive(Clone, Copy)]
pub struct Grid {
    pub width:usize,
    pub height:usize
}

#[derive(Clone, Copy)]
pub struct Tile {
    pub bounds:Rect,
    pub tile_type:TileType,
    //pub tile_effect:TileEffect
}

pub struct TileMap {
    pub map_size:Grid,
    tile_size:Grid,
    texture:Texture2D,
    tiles:Vec<Tile>,
    offset:Vec2,
    vel:Vec2,
    data:Vec<usize>,
    //loop_mode:LoopMode,
}

impl TileMap {
    
    pub fn new(
        map_size:Grid,
        tile_size:Grid,
        texture:Texture2D,
        offset:Vec2,
        vel:Vec2
        ) -> TileMap {
        
        let mapsize:usize =  map_size.width * map_size.height;
        
        let tiles_in_texture_x:usize = texture.width() as usize / tile_size.width;
        let tiles_in_texture_y:usize = texture.height() as usize / tile_size.height;

        let mut tileset:Vec<Tile> = vec!();       
        let mut tile_offset_x = 0;
        let mut tile_offset_y = 0;
        for _ in 0..(tiles_in_texture_x * tiles_in_texture_y) {
            
            let bounds = Rect::new(
                (tile_offset_x * tile_size.width) as f32,
                (tile_offset_y * tile_size.height) as f32,
                (tile_size.width) as f32,
                (tile_size.height) as f32,
            );
            tileset.push(Tile
            {
                bounds,
                tile_type:TileType::Empty,
                //tile_effect:TileEffect::Normal,
            });  
            
            tile_offset_x += 1;
            if tile_offset_x >= tiles_in_texture_x
            {
                tile_offset_y += 1; 
                tile_offset_x =0;
            }
        }
        
        println!("Adding {} tiles",tileset.len());
        
        TileMap {
            map_size,
            tile_size,
            texture,
            tiles:tileset,
            offset,
            vel,
            data:vec![0;mapsize],
            //loop_mode:LoopMode::StopBoth,
        }
    }
    
    /*pub fn update_tile(&mut self, tile:Tile)
    {
        
    }*/
    
    pub fn update(&mut self,delta:f32)
    {
        self.offset.x += self.vel.x * delta;
        self.offset.y += self.vel.y * delta;
    }
        
    fn has_collision_at_point(&self, pos_x:f32, pos_y:f32) -> Option<(f32,f32)> 
    {
        //convert position into tile
        let coord_x = f32::floor((pos_x - self.offset.x) / self.tile_size.width as f32);
        let coord_y = f32::floor((pos_y - self.offset.y) / self.tile_size.height as f32);
        let tile_pixel_x:f32 = self.offset.x + (coord_x * self.tile_size.width as f32);
        let tile_pixel_y:f32 = self.offset.y + (coord_y * self.tile_size.height as f32);
        let tile_x:usize =  coord_x as usize;
        let tile_y:usize = coord_y as usize;
        let half_tile_width = self.tile_size.width as f32 / 2.;
        let half_tile_height = self.tile_size.height as f32 / 2.;
        //return true if solid
        let target_tile:usize = tile_x + (tile_y * self.map_size.width);
        if target_tile < self.data.len() && self.tiles[self.data[target_tile]].tile_type == TileType::Solid
        {
            
            draw_rectangle(tile_pixel_x, tile_pixel_y, self.tile_size.width as f32, self.tile_size.height as f32, PINK);

            let overlap_x:f32 = if (pos_x - tile_pixel_x) > half_tile_width
            {
                (tile_pixel_x + self.tile_size.width as f32) - pos_x
            }
            else 
            {
                tile_pixel_x - pos_x
            };
            
            let overlap_y:f32 = if (pos_y - tile_pixel_y) > half_tile_height
            {
                (tile_pixel_y + self.tile_size.height as f32) - pos_y
            }
            else 
            {
                tile_pixel_y - pos_y
            };
            
            return Some((overlap_x,overlap_y));
        }
        draw_rectangle(tile_pixel_x, tile_pixel_y, self.tile_size.width as f32, self.tile_size.height as f32, GREEN);
        
        draw_rectangle(pos_x - 2., pos_y - 2., 4., 4., MAGENTA);
        
        Option::None 
    }
        
    pub fn has_collision(&self, bounds:Rect, movement:Movement) -> MovementCollision 
    {
        //change this to look in three directions based on current directions.
        // for example up = upleft, up, up right
        
        let point:(f32,f32) = match movement
        {
          Movement::Up => (bounds.center().x,bounds.top()),
          Movement::Left =>  (bounds.left(),bounds.center().y),
          Movement::Right =>  (bounds.right(),bounds.center().y),
          Movement::Down =>  (bounds.center().x,bounds.bottom()),
          Movement::DownLeft =>  (bounds.left(),bounds.bottom()),
          Movement::DownRight =>  (bounds.right(),bounds.bottom()),
          Movement::UpLeft =>  (bounds.left(),bounds.top()),
          Movement::UpRight =>  (bounds.right(),bounds.top()),
          _ => return MovementCollision::None
        };
        
        let hit =  self.has_collision_at_point(point.0, point.1);
        if let Some(overlap) = hit
        {
            println!("Collision! {:?}",movement);
            return MovementCollision::Collision(movement,overlap.0,overlap.1);
        }
        else
        {
            draw_rectangle(bounds.x, bounds.y,bounds.w,bounds.h,MAGENTA);
        }
        
        MovementCollision::None
    }
    
    pub fn update_tile(&mut self,id:usize,tile_type:TileType)
    {
        if id < self.tiles.len()
        {
            self.tiles[id].tile_type = tile_type;
        }
    }
    
    pub fn set_map(&mut self,map:&[usize])
    {
        let map_size:usize = self.map_size.width * self.map_size.height; 
        
        if map.len() >= map_size
        {
            
            /*
            for n in 0..map_size
            {
                self.data[n] = map[n];
            }
            The below code does the same as the above but clippy likes it more. I feel it is less readable
            */
            self.data[..map_size].copy_from_slice(&map[..map_size]);
        }
        else {
            println!("New map data ({}) is smaller than map size ({})!",map.len(),map_size);
        }
    }
    
    pub fn draw(&mut self)
    {
        let tex:&Texture2D = &self.texture;
        
        for x in 0..self.map_size.width 
        {
            for y in 0..self.map_size.height
            {
                let index = x + (y * self.map_size.width);
                let tile:Tile = self.tiles[self.data[index]];
                
                draw_texture_ex(
                    tex,
                    self.offset.x + (x * self.tile_size.width) as f32,
                    self.offset.y + (y * self.tile_size.height) as f32,
                    colors::WHITE,
                    DrawTextureParams{
                        //dest_size: Some(Vec2)) - use to scale - will use texture filter mode (currently always nearest neigbour ),
                        source: Some(tile.bounds),
                        ..Default::default()
                    }
                    );
            }
        }
    }
}