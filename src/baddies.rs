use macroquad::prelude::*;

use crate::{
    sprite::{
        Sprite,
        Frame,
        AnimationStyle
    },
};


#[derive(Clone,Copy)]
pub enum BaddieMode
{
    Idle = 0,
    WalkLeft,
    WalkRight,
}

pub struct Baddie
{
    common:Sprite,
    mode:BaddieMode
}

impl Baddie {
    pub fn new(texture:Texture2D, pos:Vec2, vel:Vec2) -> Baddie
    {
        let mut s:Sprite = Sprite::new(
            texture,
            pos,
            vel,
        );
        
        let idle:[Frame; 1] = [
            Frame::new(1.,0.,42.,16.,22.)
        ];
        s.add_animation(BaddieMode::Idle as usize,AnimationStyle::Loop, &idle); 
        
        Baddie {
            common:s,
            mode:BaddieMode::Idle,
        }
    }
    
    pub fn update(&mut self, delta:f32)
    {
        self.common.update_x(delta);
        self.common.update_y(delta);
        
        self.common.update_animation(delta);
    }
    
    pub fn draw(&mut self)
    {
        self.common.draw();
    }
}
