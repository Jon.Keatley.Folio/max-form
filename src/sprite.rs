use macroquad::prelude::*;

#[derive(Copy, Clone)]
pub struct Frame {
    pub bounds:Rect,
    pub delay:f32,
}

impl Frame {
    pub fn new(delay:f32,x:f32,y:f32,w:f32,h:f32) -> Self 
    {
        Frame {
            delay,
            bounds:Rect::new(x,y,w,h)
        }
    }
}

pub enum AnimationStyle {
    End,
    Loop,
    //Bounce,
}

pub struct Animation {
    counter:f32,
    frame:usize,
    frames:Vec<Frame>,
    style:AnimationStyle,
    has_stopped:bool
}

impl Animation {
    
    pub fn blank() -> Animation
    {
        Animation{
            frames:vec![],
            counter:0.,
            frame:0,
            style:AnimationStyle::End,
            has_stopped:true
        }
    }
    
    pub fn update(&mut self,delta:f32)
    {
        if self.has_stopped
        {
            return;
        }
        
        self.counter += delta;
        if self.counter >= self.frames[self.frame].delay
        {
            self.counter = 0.;
            if self.frame + 1 >= self.frames.len()
            {
                //TODO: implement bounce
                match &mut self.style {
                    AnimationStyle::End => {
                        self.has_stopped = true;
                    },
                    AnimationStyle::Loop => {
                        self.frame = 0;
                    },
                }
            }
            else
            {
                self.frame += 1;
            }
        }
    }
    
    pub fn get_draw_bounds(&self) -> Rect {
        self.frames[self.frame].bounds
    }
}

pub struct Sprite {
    texture: Texture2D,
    pub pos:Vec2,
    pub vel:Vec2,
    animations:Vec<Animation>,
    current_animation:usize,
}

impl Sprite {
    
    pub fn new(
        texture:Texture2D,
        pos:Vec2,
        vel:Vec2
    ) -> Self {
        Sprite {
            texture,
            pos,
            vel,
            current_animation:0,
            animations:vec![]
        }
    }
    
    
    pub fn add_animation(&mut self, id:usize, animation_style:AnimationStyle, frames:&[Frame])
    {
        let mut f: Vec<Frame> = vec![];
        
        if self.animations.len() <= id
        {
            while self.animations.len() <= id
            {
                self.animations.push(Animation::blank());
            }
        }
        
        //TODO find a nicer way to add frames
        for frame in frames
        {
            let clone = *frame;
            f.push(clone);
        }
 
        self.animations[id] = Animation{
            frames:f,
            counter:0.,
            frame:0,
            style:animation_style,
            has_stopped:false
        };
    }
    
    pub fn change_animation(&mut self, id:usize)
    {
        self.current_animation = id;
    }
    
    pub fn update_animation(&mut self,delta:f32) {
        //self.pos.x += self.vel.x * delta;
        //self.pos.y += self.vel.y * delta;
        
        self.animations[self.current_animation].update(delta);
        
    }
    
    pub fn update_x(&mut self, delta:f32) {
        self.pos.x += self.vel.x * delta;
    }
    
    pub fn update_y(&mut self, delta:f32) {
        self.pos.y += self.vel.y * delta;
    }
    
    pub fn undo_update_x(&mut self, delta:f32)
    {
         self.pos.x -= self.vel.x * delta;
    }
    
    pub fn undo_update_y(&mut self, delta:f32)
    {
         self.pos.y -= self.vel.y * delta;
    }
    
    pub fn alter_position(&mut self,mod_x:f32,mod_y:f32)
    {
        if self.vel.x != 0.
        {
            self.pos.x += mod_x;
        }
        
        if self.vel.y != 0.
        {
            self.pos.y += mod_y;
        }
    }
    
    pub fn draw(&mut self) {
        let tex:&Texture2D = &self.texture;
        
        let bounds =self.animations[self.current_animation].get_draw_bounds();
        
        draw_texture_ex(
            tex,
            self.pos.x,
            self.pos.y, 
            WHITE,
            DrawTextureParams{
                //dest_size: Some(Vec2)) - use to scale - will use texture filter mode (currently always nearest neigbour ),
                source: Some(bounds),
                ..Default::default()
            }
            );
    }
    
    pub fn get_bounds(&self) -> Rect 
    {
        let padding:f32 = 2.;
        let mut bounds = self.animations[self.current_animation].get_draw_bounds();
        bounds.x = self.pos.x - padding; 
        bounds.y = self.pos.y - padding;
        bounds.w += padding;
        bounds.h += padding;
        
        bounds
        
    }
}

