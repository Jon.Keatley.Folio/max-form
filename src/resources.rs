
use macroquad::Error;
use macroquad::texture::{
    Texture2D,
    FilterMode,
    load_texture,
};

pub struct Resources {
    pub player: Texture2D,
    pub tiles : Texture2D,
    pub baddie: Texture2D,
}


impl Resources {
    pub async fn load_image(path: &str) -> Result<Texture2D, Error> {
        
        let texture: Texture2D = load_texture(path).await?;
        texture.set_filter(FilterMode::Nearest);
        
        Ok(texture)
    }
}

pub async fn load_resources() -> Resources {

    let p = Resources::load_image("resources/slime.png").await.expect("Could not load player sprite sheet");
    let t = Resources::load_image("resources/scifitiles-sheet.png").await.expect("Unable to load tileset"); 
    let b = Resources::load_image("resources/bot.png").await.expect("Could not load baddies");
        
    Resources {
        player: p,
        tiles: t,
        baddie: b,
    } 
}