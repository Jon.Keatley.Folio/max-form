# max-form

A game written in rust in the hope that at the end I will know rust and have a new game.

[Check it out!](https://max-form-jon-keatley-folio-d3682260cf86ff99c32c18c0b4692089027b.gitlab.io/)

## Todo

- [x] something on screen
- [x] WASM CI, gitlab page CD
- [x] sprite
- [x] tile map 
- [x] map collision v1
- [x] shooting
- [ ] bad guys
- [ ] collision
- [ ] bad guy ai
- [ ] attacks
- [ ] particles 
- [ ] animated map tiles
- [ ] map collision v2
- [ ] screens / game modes
- [ ] controls
- [ ] resolution scaling
- [ ] loading in maps
- [ ] scrolling map
- [ ] camera
- [ ] cut scene engine
- [ ] sound (look how low it is on my list!)

## Tools 

- [ ] look into libre sprite
- [ ] 8-bit wav creator
- [ ] level editor


## coding resouces

- https://gamedev.net/articles/programming/general-and-gameplay-programming/swept-aabb-collision-detection-and-response-r3084/
- https://gamedev.stackexchange.com/questions/23173/how-can-i-determine-which-direction-a-2d-collision-is-occurring-from
- https://github.com/adambiltcliffe/roguelike/blob/main/map/src/geom.rs

## used art resources 
- [Placeholder tiles](https://opengameart.org/content/sci-fi-interior-tiles) created by Buch http://blog-buch.rhcloud.com
- [Placeholder character](https://opengameart.org/content/slime-4)
- [Placeholder baddies](https://opengameart.org/content/future-robot)